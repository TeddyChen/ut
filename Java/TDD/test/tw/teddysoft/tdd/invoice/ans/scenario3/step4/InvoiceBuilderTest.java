package tw.teddysoft.tdd.invoice.ans.scenario3.step4;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by teddy on 2017/3/27.
 */
public class InvoiceBuilderTest {

    @Test
    public void calc_tax_excluded_price_with_tax_included_price(){
        // expected = 94.28 -> 94
        assertThat(InvoiceBuilder.calcTaxExcludedPrice(99, 0.05)).isEqualTo(94);

        // expected = 104.76 -> 105
        assertThat(InvoiceBuilder.calcTaxExcludedPrice(110, 0.05)).isEqualTo(105);
    }

    @Test
    public void calc_vat_with_tax_included_price(){
        assertThat(InvoiceBuilder.calcVat(99, 0.05)).isEqualTo(5);
        assertThat(InvoiceBuilder.calcVat(110, 0.05)).isEqualTo(5);
    }

    @Test
    public void when_tax_included_price_under_10_then_tax_free(){
        assertThat(InvoiceBuilder.calcVat(0, 0.05)).isEqualTo(0);
        assertThat(InvoiceBuilder.calcVat(1, 0.05)).isEqualTo(0);
        assertThat(InvoiceBuilder.calcVat(2, 0.05)).isEqualTo(0);
        assertThat(InvoiceBuilder.calcVat(3, 0.05)).isEqualTo(0);
        assertThat(InvoiceBuilder.calcVat(4, 0.05)).isEqualTo(0);
        assertThat(InvoiceBuilder.calcVat(5, 0.05)).isEqualTo(0);
        assertThat(InvoiceBuilder.calcVat(6, 0.05)).isEqualTo(0);
        assertThat(InvoiceBuilder.calcVat(7, 0.05)).isEqualTo(0);
        assertThat(InvoiceBuilder.calcVat(8, 0.05)).isEqualTo(0);
        assertThat(InvoiceBuilder.calcVat(9, 0.05)).isEqualTo(0);
        assertThat(InvoiceBuilder.calcVat(10, 0.05)).isEqualTo(0);
    }
    
    @Test
    public void when_tax_included_price_is_11_then_vat_is_1(){
        assertThat(InvoiceBuilder.calcVat(11, 0.05)).isEqualTo(1);
    }

}

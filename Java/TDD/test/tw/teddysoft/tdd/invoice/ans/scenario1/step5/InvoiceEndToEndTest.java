package tw.teddysoft.tdd.invoice.ans.scenario1.step5;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by teddy on 2017/3/27.
 */
public class InvoiceEndToEndTest {
    InvoiceApp app;

    @Before
    public void setUp(){
        app = new InvoiceApp();
    }

    @After
    public void tearDown(){
        if (null != app)
            app.stop();
    }

    @Test
    public void issuing_invoice_with_tax_included_price(){
        app.start();
        app.collectingInput();

        int taxIncludedPrice = app.getTaxIncludedPrice();
        double vatRate = app.getVatRate();

        Invoice invoice = InvoiceBuilder.newInstance().
                withTaxIncludedPrice(taxIncludedPrice).
                withVatRate(vatRate).
                issue();

        assertThat(invoice).isNotNull();
        assertThat(invoice.getTaxIncludedPrice()).isEqualTo(17000);
        assertThat(invoice.getVat()).isEqualTo(810);
        assertThat(invoice.getTaxExcludedPrice()).isEqualTo(16190);
        assertThat(invoice.getVatRate()).isEqualTo(0.05);

        app.show(invoice);
    }

}

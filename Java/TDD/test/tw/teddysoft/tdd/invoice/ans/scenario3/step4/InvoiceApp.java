package tw.teddysoft.tdd.invoice.ans.scenario3.step4;

/**
 * Created by teddy on 2017/3/27.
 */
public class InvoiceApp {
    private int taxIncludedPrice;
    private double vatRate;
    
    public void stop() {
    }

    public void start() {
    }

    public void collectingInput(int taxIncludedPrice, double vatRate) {
        this.taxIncludedPrice = taxIncludedPrice;
        this.vatRate = vatRate;
    }

    public int getTaxIncludedPrice() {
        return taxIncludedPrice;
    }

    public double getVatRate() {
        return vatRate;
    }

    public void show(Invoice invoice) {
    }
}

package tw.teddysoft.tdd.invoice.ans.scenario2.step2;

/**
 * Created by teddy on 2017/3/27.
 */
public class InvoiceApp {
    private int taxIncludedPrice;
    private double vatRate;
    
    public void stop() {
    }

    public void start() {
    }

    public void collectingInput() {
    }


    public int getTaxIncludedPrice() {
        taxIncludedPrice = 99;
        return taxIncludedPrice;
    }

    public double getVatRate() {
        vatRate = 0.05;
        return vatRate;
    }

    public void show(Invoice invoice) {
    }
}

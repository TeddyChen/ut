package tw.teddysoft.tdd.invoice.ans.scenario3.step2;

/**
 * Created by teddy on 2017/3/27.
 */
public class Invoice {
    private int taxIncludedPrice;
    private int vat;
    private int taxExcludedPrice;
    private double vatRate;

    public Invoice(int taxIncludedPrice, int vat, int taxExcludedPrice, double vatRate) {
        this.taxIncludedPrice = taxIncludedPrice;
        this.vat = vat;
        this.taxExcludedPrice = taxExcludedPrice;
        this.vatRate = vatRate;

    }

    public int getTaxIncludedPrice() {
        return taxIncludedPrice;
    }

    public int getVat() {
        return vat;
    }

    public int getTaxExcludedPrice() {
        return taxExcludedPrice;
    }

    public double getVatRate() {
        return vatRate;
    }
}

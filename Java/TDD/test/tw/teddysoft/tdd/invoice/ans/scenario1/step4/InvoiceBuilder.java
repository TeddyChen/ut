package tw.teddysoft.tdd.invoice.ans.scenario1.step4;

/**
 * Created by teddy on 2017/3/27.
 */
public class InvoiceBuilder {

    public static InvoiceBuilder newInstance() {
        return new InvoiceBuilder();
    }

    public InvoiceBuilder withTaxIncludedPrice(int taxIncludedPrice) {
        return this;
    }

    public InvoiceBuilder withVatRate(double vatRate) {
        return this;
    }

    public Invoice issue() {
        return null;
    }
}

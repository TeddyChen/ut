package tw.teddysoft.tdd.invoice.ans.scenario3.step4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by teddy on 2017/3/27.
 */
public class InvoiceEndToEndTest {
    InvoiceApp app;
    Invoice invoice;

    @Before
    public void setUp(){
        app = new InvoiceApp();
        app.start();
    }

    @After
    public void tearDown(){
        if (null != app){
            app.show(invoice);
            app.stop();
        }
    }

    @Test
    public void issuing_invoice_with_tax_included_price(){
        app.collectingInput(17000, 0.05);

        int taxIncludedPrice = app.getTaxIncludedPrice();
        double vatRate = app.getVatRate();

        invoice = InvoiceBuilder.newInstance().
                withTaxIncludedPrice(taxIncludedPrice).
                withVatRate(vatRate).
                issue();

        assertThat(invoice).isNotNull();
        assertThat(invoice.getTaxIncludedPrice()).isEqualTo(17000);
        assertThat(invoice.getVat()).isEqualTo(810);
        assertThat(invoice.getTaxExcludedPrice()).isEqualTo(16190);
        assertThat(invoice.getVatRate()).isEqualTo(0.05);
    }

    @Test
    public void issuing_invoice_with_tax_included_price_round_example(){
        app.collectingInput(99, 0.05);

        int taxIncludedPrice = app.getTaxIncludedPrice();
        double vatRate = app.getVatRate();

        invoice = InvoiceBuilder.newInstance().
                withTaxIncludedPrice(taxIncludedPrice).
                withVatRate(vatRate).
                issue();

        assertThat(invoice).isNotNull();
        assertThat(invoice.getTaxIncludedPrice()).isEqualTo(99);
        assertThat(invoice.getVat()).isEqualTo(5);
        assertThat(invoice.getTaxExcludedPrice()).isEqualTo(94);
        assertThat(invoice.getVatRate()).isEqualTo(0.05);
    }

    @Test
    public void should_be_tax_free_when_tax_included_price_is_10(){
        app.collectingInput(10, 0.05);

        invoice = InvoiceBuilder.newInstance().
                withTaxIncludedPrice(app.getTaxIncludedPrice()).
                withVatRate(app.getVatRate()).
                issue();

        verifyInvoice(10, 0, 10, 0.05);
    }

    //Refactored test cases
    private void verifyInvoice( int taxIncludedPrice, int vat, int TaxExcluded, double vatRate){
        assertThat(invoice).isNotNull();
        assertThat(invoice.getTaxIncludedPrice()).isEqualTo(taxIncludedPrice);
        assertThat(invoice.getVat()).isEqualTo(vat);
        assertThat(invoice.getTaxExcludedPrice()).isEqualTo(TaxExcluded);
        assertThat(invoice.getVatRate()).isEqualTo(vatRate);
    }


}

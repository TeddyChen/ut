package tw.teddysoft.tdd.invoice.ans.scenario4.step3;

/**
 * Created by teddy on 2017/3/27.
 */
public class InvoiceApp {
    private int taxIncludedPrice;
    private double vatRate;
    private int taxExcludedPrice;

    public void stop() {
    }

    public void start() {
    }

    public void collectingInput(int taxIncludedPrice, double vatRate, int taxExcludedPrice) {
        this.taxIncludedPrice = taxIncludedPrice;
        this.vatRate = vatRate;
        this.taxExcludedPrice = taxExcludedPrice;
    }

    public int getTaxIncludedPrice() {
        return taxIncludedPrice;
    }

    public double getVatRate() {
        return vatRate;
    }

    public int getTaxExcludedPrice() {
        return taxExcludedPrice;
    }

    public void show(Invoice invoice) {
    }
}

package tw.teddysoft.tdd.invoice.ans.scenario1.step6;

/**
 * Created by teddy on 2017/3/27.
 */
public class InvoiceApp {
    private int taxIncludedPrice;
    private double vatRate;
    
    public void stop() {
    }

    public void start() {
    }

    public void collectingInput() {
    }


    public int getTaxIncludedPrice() {
        return taxIncludedPrice;
    }

    public double getVatRate() {
        return vatRate;
    }

    public void show(Invoice invoice) {
    }
}

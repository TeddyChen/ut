package tw.teddysoft.tdd.invoice.ans.scenario3.step3;

/**
 * Created by teddy on 2017/3/27.
 */
public class InvoiceBuilder {
    private int taxIncludedPrice;
    private double vatRate;

    public static InvoiceBuilder newInstance() {
        return new InvoiceBuilder();
    }

    public InvoiceBuilder withTaxIncludedPrice(int taxIncludedPrice) {
        this.taxIncludedPrice = taxIncludedPrice;
        return this;
    }

    public InvoiceBuilder withVatRate(double vatRate) {
        this.vatRate = vatRate;
        return this;
    }

    public Invoice issue() {
        return new Invoice(taxIncludedPrice,
                calcVat(taxIncludedPrice, vatRate),
                calcTaxExcludedPrice(taxIncludedPrice, vatRate), vatRate);
    }

    //Fix the bug
    public static int calcVat(int taxIncludedPrice, double vatRate){
//        return (int) Math.round(calcTaxExcludedPrice(taxIncludedPrice, vatRate) * vatRate);
        return  taxIncludedPrice - calcTaxExcludedPrice(taxIncludedPrice, vatRate);
    }

    public static int calcTaxExcludedPrice(int taxIncludedPrice, double vatRate){
        return (int) Math.round(taxIncludedPrice / (1 + vatRate));
    }

}

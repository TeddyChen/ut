package tw.teddysoft.tdd.invoice.ans.scenario1.step5;

/**
 * Created by teddy on 2017/3/27.
 */
public class Invoice {
    private int taxIncludedPrice;
    private int vat;
    private int taxExcludedPrice;
    private double vatRate;

    public int getTaxIncludedPrice() {
        return taxIncludedPrice;
    }

    public int getVat() {
        return vat;
    }

    public int getTaxExcludedPrice() {
        return taxExcludedPrice;
    }

    public double getVatRate() {
        return vatRate;
    }
}

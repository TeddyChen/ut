package tw.teddysoft.tdd.invoice.ans.scenario3.step1;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by teddy on 2017/3/27.
 */
public class InvoiceEndToEndTest {
    InvoiceApp app;

    @Before
    public void setUp(){
        app = new InvoiceApp();
    }

    @After
    public void tearDown(){
        if (null != app)
            app.stop();
    }

    @Test
    public void issuing_invoice_with_tax_included_price(){
        app.start();
        app.collectingInput(17000, 0.05);

        int taxIncludedPrice = app.getTaxIncludedPrice();
        double vatRate = app.getVatRate();

        Invoice invoice = InvoiceBuilder.newInstance().
                withTaxIncludedPrice(taxIncludedPrice).
                withVatRate(vatRate).
                issue();

        assertThat(invoice).isNotNull();
        assertThat(invoice.getTaxIncludedPrice()).isEqualTo(17000);
        assertThat(invoice.getVat()).isEqualTo(810);
        assertThat(invoice.getTaxExcludedPrice()).isEqualTo(16190);
        assertThat(invoice.getVatRate()).isEqualTo(0.05);

        app.show(invoice);
    }

    @Test
    public void issuing_invoice_with_tax_included_price_round_example(){
        app.start();
        app.collectingInput(99, 0.05);

        int taxIncludedPrice = app.getTaxIncludedPrice();
        double vatRate = app.getVatRate();

        Invoice invoice = InvoiceBuilder.newInstance().
                withTaxIncludedPrice(taxIncludedPrice).
                withVatRate(vatRate).
                issue();

        assertThat(invoice).isNotNull();
        assertThat(invoice.getTaxIncludedPrice()).isEqualTo(99);
        assertThat(invoice.getVat()).isEqualTo(5);
        assertThat(invoice.getTaxExcludedPrice()).isEqualTo(94);
        assertThat(invoice.getVatRate()).isEqualTo(0.05);

        app.show(invoice);
    }

    // This test case fails. In step 2, write unit test to verify the InvoiceBuilder logic
    @Test
    public void should_be_tax_free_when_tax_included_price_is_10(){
        app.start();
        app.collectingInput(10, 0.05);

        int taxIncludedPrice = app.getTaxIncludedPrice();
        double vatRate = app.getVatRate();

        Invoice invoice = InvoiceBuilder.newInstance().
                withTaxIncludedPrice(taxIncludedPrice).
                withVatRate(vatRate).
                issue();

        assertThat(invoice).isNotNull();
        assertThat(invoice.getTaxIncludedPrice()).isEqualTo(10);
        assertThat(invoice.getVat()).isEqualTo(0);
        assertThat(invoice.getTaxExcludedPrice()).isEqualTo(10);
        assertThat(invoice.getVatRate()).isEqualTo(0.05);

        app.show(invoice);
    }




}

package tw.teddysoft.tdd.invoice.ans.scenario2.step2;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by teddy on 2017/3/27.
 */
public class InvoiceBuilderTest {

    @Test
    public void calc_tax_excluded_price_with_tax_included_price(){
        // expected = 94.28 -> 94
        assertThat(InvoiceBuilder.calcTaxExcludedPrice(99, 0.05)).isEqualTo(94);

        // expected = 104.76 -> 105
        assertThat(InvoiceBuilder.calcTaxExcludedPrice(110, 0.05)).isEqualTo(105);
    }

    @Test
    public void calcVat_with_tax_included_price(){
        assertThat(InvoiceBuilder.calcVat(99, 0.05)).isEqualTo(5);
        assertThat(InvoiceBuilder.calcVat(110, 0.05)).isEqualTo(5);
    }


}

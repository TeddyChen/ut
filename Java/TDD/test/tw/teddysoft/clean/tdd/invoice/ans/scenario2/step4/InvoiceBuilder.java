package tw.teddysoft.clean.tdd.invoice.ans.scenario2.step4;

public class InvoiceBuilder {

    private int taxIncludedPrice;
    private double vatRate;

    public void withTaxIncludedPrice(int taxIncludedPrice) {
        this.taxIncludedPrice = taxIncludedPrice;
    }

    public void withVatRate(double vatRate) {
        this.vatRate = vatRate;
    }

    public Invoice issue() {
        return new Invoice(taxIncludedPrice,
                calcVat(taxIncludedPrice, vatRate),
                calcTaxExcludedPrice(taxIncludedPrice, vatRate), vatRate);

    }

    public static int calcVat(int taxIncludedPrice, double vatRate){
        return (int) Math.round(calcTaxExcludedPrice(taxIncludedPrice, vatRate) * vatRate);
    }

    public static int calcTaxExcludedPrice(int taxIncludedPrice, double vatRate){
        return (int) Math.round(taxIncludedPrice / (1 + vatRate));
    }

}

package tw.teddysoft.clean.tdd.invoice.ans.scenario2.step4;

public class Invoice {

    private int taxExcludedPrice;
    private int taxIncludedPrice;
    private double varRate;
    private int vat;

    public Invoice(int taxIncludedPrice, int vat, int taxExcludedPrice, double vatRate) {
        this.taxIncludedPrice = taxIncludedPrice;
        this.vat = vat;
        this.taxExcludedPrice = taxExcludedPrice;
        this.varRate = vatRate;
    }

    public int getTaxExcludedPrice(){
        return taxExcludedPrice;
    }

    public int getTaxIncludedPrice() {
        return taxIncludedPrice;
    }

    public double getVatRate() {
        return varRate;
    }

    public int getVat() { return vat; }
}

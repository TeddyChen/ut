package tw.teddysoft.clean.tdd.invoice.ans.scenario1.step3;

public class IssueInvoiceInput {

    private int taxIncludedPrice;
    private double vatRate;

    public void setTaxIncludedPrice(int taxIncludedPrice) {
        this.taxIncludedPrice = taxIncludedPrice;
    }

    public void setVatRate(double vatRate) {
        this.vatRate = vatRate;
    }

    public int getTaxIncludedPrice() {
        return taxIncludedPrice;
    }

    public double getVatRate() {
        return vatRate;
    }
}

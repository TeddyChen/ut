package tw.teddysoft.clean.tdd.invoice.ans.scenario2.step3;

public class InvoiceBuilder {
    public void withTaxIncludedPrice(int taxIncludedPrice) {
    }

    public void withVatRate(double vatRate) {
    }

    public Invoice issue() {
        return new Invoice();
    }
}

package tw.teddysoft.clean.tdd.invoice.ans.scenario2.step4;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

public class InvoiceBuilderTest {

    @Test
    public void calc_tax_excluded_price_with_tax_included_price(){
        // expected = 94.28 -> 94
        assertEquals(94, InvoiceBuilder.calcTaxExcludedPrice(99, 0.05));

        // expected = 104.76 -> 105
        assertEquals(105, InvoiceBuilder.calcTaxExcludedPrice(110, 0.05));
    }

    @Test
    public void calcVat_with_tax_included_price(){
        assertEquals(5, InvoiceBuilder.calcVat(99, 0.05));
        assertEquals(5, InvoiceBuilder.calcVat(110, 0.05));
    }
}

package tw.teddysoft.clean.tdd.invoice.ans.scenario2.step4;

public class IssueInvoiceOutput {
    private double vatRate;
    private int taxIncludedPrice;
    private int taxExcludedPrice;
    private int vat;

    public double getVatRate() {
        return vatRate;
    }

    public int getTaxIncludedPrice() {
        return taxIncludedPrice;
    }

    public int getTaxExcludedPrice() {
        return taxExcludedPrice;
    }

    public int getVat() {
        return vat;
    }

    public void setTaxIncludedPrice(int taxIncludedPrice) {
        this.taxIncludedPrice = taxIncludedPrice;
    }

    public void setVatRate(double vatRate) {
        this.vatRate = vatRate;
    }

    public void setTaxExcludedPrice(int taxExcludedPrice) {
        this.taxExcludedPrice = taxExcludedPrice;
    }

    public void setVat(int vat) {
        this.vat = vat;
    }
}

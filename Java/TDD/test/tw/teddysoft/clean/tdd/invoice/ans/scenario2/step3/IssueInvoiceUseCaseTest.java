package tw.teddysoft.clean.tdd.invoice.ans.scenario2.step3;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IssueInvoiceUseCaseTest {

    @Test
    public void withTaxIncludedPrice(){

        IssueInvoiceUseCase issueInvoiceUseCase = new IssueInvoiceUseCase();

        IssueInvoiceInput input = new IssueInvoiceInput();
        input.setTaxIncludedPrice(17000);
        input.setVatRate(0.05);

        IssueInvoiceOutput output = new IssueInvoiceOutput();

        issueInvoiceUseCase.execute(input, output);

        assertEquals(0.05, output.getVatRate(), 0.0001);
        assertEquals(17000, output.getTaxIncludedPrice());
        assertEquals(16190, output.getTaxExcludedPrice());
        assertEquals(810, output.getVat());

    }


    @Test
    public void withTaxIncludedPriceRound(){

        IssueInvoiceUseCase issueInvoiceUseCase = new IssueInvoiceUseCase();

        IssueInvoiceInput input = new IssueInvoiceInput();
        input.setTaxIncludedPrice(99);
        input.setVatRate(0.05);

        IssueInvoiceOutput output = new IssueInvoiceOutput();

        issueInvoiceUseCase.execute(input, output);

        assertEquals(0.05, output.getVatRate(), 0.0001);
        assertEquals(99, output.getTaxIncludedPrice());
        assertEquals(94, output.getTaxExcludedPrice());
        assertEquals(5, output.getVat());

    }

}

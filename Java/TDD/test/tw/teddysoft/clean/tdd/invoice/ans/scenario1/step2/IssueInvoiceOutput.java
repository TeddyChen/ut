package tw.teddysoft.clean.tdd.invoice.ans.scenario1.step2;

public class IssueInvoiceOutput {
    public double getVatRate() {
        return 0;
    }

    public int getTaxIncludedPrice() {
        return 0;
    }

    public int getTaxExcludedPrice() {
        return 0;
    }

    public int getVat() {
        return 0;
    }
}

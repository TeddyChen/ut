package tw.teddysoft.spy.ans;

import org.junit.Test;
import tw.teddysoft.monitor.Alert;
import tw.teddysoft.monitor.Door;
import tw.teddysoft.monitor.DoorCommand;
import tw.teddysoft.monitor.Server;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by teddy on 2017/3/27.
 */
public class WhenServerSendAlert {

    @Test
    public void server_will_send_an_alert_when_a_command_execution_result_is_critical(){
        AlertSpy spy = new AlertSpy();
        Server server = new Server(spy);
        server.addCommand(new DoorCommand(new OpenDoorStub()));
        server.monitor();
        assertThat(spy.sentCount).isEqualTo(1);

        server.addCommand(new DoorCommand(new OpenDoorStub()));
        server.monitor();
        assertThat(spy.sentCount).isEqualTo(3);
    }

    @Test
    public void server_will_not_send_an_alert_when_a_command_execution_result_is_ok(){
        AlertSpy spy = new AlertSpy();
        Server server = new Server(spy);
        server.addCommand(new DoorCommand(new ClosedDoorStub()));
        server.monitor();
        assertThat(spy.sentCount).isEqualTo(0);

        server.addCommand(new DoorCommand(new ClosedDoorStub()));
        server.monitor();
        assertThat(spy.sentCount).isEqualTo(0);
    }

    public class AlertSpy implements Alert{
        private int sentCount = 0;

        @Override
        public void sendAlert(String msg) {
            sentCount++;
        }

        @Override
        public String getAllSentContent() {
            return null;
        }

        public int getSentCount(){
            return sentCount;
        }
    }


    public class OpenDoorStub implements Door {
        @Override
        public String getStatus() {
            return "open";
        }
    }

    public class ClosedDoorStub implements Door {
        @Override
        public String getStatus() {
            return "not open";
        }
    }

}

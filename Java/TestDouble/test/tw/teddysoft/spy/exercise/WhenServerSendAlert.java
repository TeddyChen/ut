package tw.teddysoft.spy.exercise;

import org.junit.Test;
import tw.teddysoft.monitor.Door;

/**
 * Created by teddy on 2017/3/27.
 */
public class WhenServerSendAlert {

    // TODO: Implement AlertSpy class to make the two test methods work

    @Test
    public void server_will_send_an_alert_when_a_command_execution_result_is_critical(){
//        AlertSpy spy = new AlertSpy();
//        Server server = new Server(spy);
//        server.addCommand(new DoorCommand(new OpenDoorStub()));
//        server.monitor();
//        assertThat(spy.sendCount).isEqualTo(1);
//
//        server.addCommand(new DoorCommand(new OpenDoorStub()));
//        server.monitor();
//        assertThat(spy.sendCount).isEqualTo(3);
    }

    @Test
    public void server_will_not_send_an_alert_when_a_command_execution_result_is_ok(){
//        AlertSpy spy = new AlertSpy();
//        Server server = new Server(spy);
//        server.addCommand(new DoorCommand(new ClosedDoorStub()));
//        server.monitor();
//        assertThat(spy.sendCount).isEqualTo(0);
//
//        server.addCommand(new DoorCommand(new ClosedDoorStub()));
//        server.monitor();
//        assertThat(spy.sendCount).isEqualTo(0);
    }

    public class OpenDoorStub implements Door {
        @Override
        public String getStatus() {
            return "open";
        }
    }

    public class ClosedDoorStub implements Door {
        @Override
        public String getStatus() {
            return "not open";
        }
    }

}

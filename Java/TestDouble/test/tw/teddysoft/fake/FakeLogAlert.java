package tw.teddysoft.fake;

import tw.teddysoft.monitor.Alert;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Created by teddy on 2017/3/27.
 */
public class FakeLogAlert implements Alert {
    private final String fileName;

    public FakeLogAlert(String fileName) throws IOException {
        this.fileName = fileName;
    }

    @Override
    public void sendAlert(String msg) {
        msg = msg + "\n";
        try {
            Files.write(Paths.get(fileName), msg.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getAllSentContent() {
        byte[] contents = new byte[0];
        try {
            contents = Files.readAllBytes(Paths.get(fileName));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return new String(contents);
    }
}

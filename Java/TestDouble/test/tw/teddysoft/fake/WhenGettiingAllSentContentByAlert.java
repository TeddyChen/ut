package tw.teddysoft.fake;

import org.junit.Before;
import org.junit.Test;
import tw.teddysoft.monitor.Door;
import tw.teddysoft.monitor.DoorCommand;
import tw.teddysoft.monitor.Server;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by teddy on 2017/3/27.
 */
public class WhenGettiingAllSentContentByAlert {
    private static final String FILE_NAME = "fakelog.txt";

    @Before
    public void setUp() throws IOException{
        File file = new File(FILE_NAME);
        if(file.exists()){
            Files.delete(Paths.get(FILE_NAME));
        }
        file.createNewFile();
    }

    @Test
    public void should_get_three_items_when_add_three_door_open_commands() throws IOException {
        FakeLogAlert alert = new FakeLogAlert(FILE_NAME);
        Server server = new Server(alert);
        server.addCommand(new DoorCommand(new OpenDoorStub()));
        server.addCommand(new DoorCommand(new OpenDoorStub()));
        server.addCommand(new DoorCommand(new OpenDoorStub()));
        server.monitor();

        StringBuilder expected  = new StringBuilder();
        expected.append("Door is open.").append("\n");
        expected.append("Door is open.").append("\n");
        expected.append("Door is open.").append("\n");
        assertThat(alert.getAllSentContent()).isEqualTo(expected.toString());
    }


    @Test
    public void should_get_empty_content_when_add_three_door_closed_commands() throws IOException {
        FakeLogAlert alert = new FakeLogAlert(FILE_NAME);
        Server server = new Server(alert);
        server.addCommand(new DoorCommand(new ClosedDoorStub()));
        server.addCommand(new DoorCommand(new ClosedDoorStub()));
        server.addCommand(new DoorCommand(new ClosedDoorStub()));
        server.monitor();

        assertThat(alert.getAllSentContent()).isEqualTo("");
    }

    public class OpenDoorStub implements Door {
        @Override
        public String getStatus() {
            return "open";
        }
    }

    public class ClosedDoorStub implements Door {
        @Override
        public String getStatus() {
            return "not open";
        }
    }

}

package tw.teddysoft.mock.exercise;

import org.junit.Test;
import org.mockito.Mockito;
import tw.teddysoft.monitor.Alert;
import tw.teddysoft.monitor.Command;
import tw.teddysoft.monitor.Result;
import tw.teddysoft.monitor.Server;

import static org.mockito.Mockito.*;

/**
 * Created by teddy on 2017/3/27.
 */
public class WhenServerSendAlert {

    @Test
    public void server_will_send_an_alert_when_a_command_execution_result_is_critical(){
        Alert spy = mock(Alert.class);

        Command mockCmd = Mockito.mock(Command.class);
        Mockito.when(mockCmd.execute()).thenReturn(new Result(Result.CRITICAL, "Door is open."));

        Server server = new Server(spy);
        server.addCommand(mockCmd);
        server.monitor();
        Mockito.verify(spy, times(1)).sendAlert("Door is open.");

        server.addCommand(mockCmd);
        server.monitor();
        Mockito.verify(spy, times(3)).sendAlert("Door is open.");
    }

    @Test
    public void server_will_not_send_an_alert_when_a_command_execution_result_is_ok(){
        Alert spy = mock(Alert.class);

        Command mockCmd = Mockito.mock(Command.class);
        Mockito.when(mockCmd.execute()).thenReturn(new Result(Result.OK, "Door is closed."));

        Server server = new Server(spy);
        server.addCommand(mockCmd);
        server.monitor();
        verify(spy, never()).sendAlert("Door is closed.");

        server.addCommand(mockCmd);
        server.monitor();
        verify(spy, never()).sendAlert("Door is closed.");
    }
}

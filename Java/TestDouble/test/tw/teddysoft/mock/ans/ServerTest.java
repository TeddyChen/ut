package tw.teddysoft.mock.ans;

import org.junit.Test;
import org.mockito.Mockito;
import tw.teddysoft.monitor.Alert;
import tw.teddysoft.monitor.Command;
import tw.teddysoft.monitor.Server;

import static org.assertj.core.api.Assertions.assertThat;

public class ServerTest {
	@Test
	public void command_size_are_two_when_add_two_commands_to_server(){

		Command mockCmd = Mockito.mock(Command.class);
		Alert alert = Mockito.mock(Alert.class);

		Server server = new Server(alert);
		assertThat(server.getCommandSize()).isEqualTo(0);

		server.addCommand(mockCmd);
		server.addCommand(mockCmd);

		assertThat(server.getCommandSize()).isEqualTo(2);
	}
}

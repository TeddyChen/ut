package tw.teddysoft.mock.ans;

import org.junit.Test;
import org.mockito.Mockito;
import tw.teddysoft.monitor.Door;
import tw.teddysoft.monitor.DoorCommand;
import tw.teddysoft.monitor.Result;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * Created by teddy on 2017/3/27.
 */
public class CommandTest {

    @Test
    public void result_is_critical_when_door_is_open(){
        Door mockDoor = Mockito.mock(Door.class);
        Mockito.when(mockDoor.getStatus()).thenReturn("open");

        DoorCommand doorCmd =  new DoorCommand(mockDoor);
        Result result = doorCmd.execute();
        assertThat(result.getStatus()).isEqualTo(Result.CRITICAL);
        assertThat(result.getMessage()).startsWith("Door is open");
    }

    @Test
    public void result_is_OK_when_door_is_not_open(){
        Door mockDoor = Mockito.mock(Door.class);
        Mockito.when(mockDoor.getStatus()).thenReturn("not open");

        DoorCommand doorCmd =  new DoorCommand(mockDoor);
        Result result = doorCmd.execute();
        assertThat(result.getStatus()).isEqualTo(Result.OK);
        assertThat(result.getMessage()).startsWith("Your home is safe");
    }
}

package tw.teddysoft.stub.ans;

import org.junit.Test;
import tw.teddysoft.monitor.Door;
import tw.teddysoft.monitor.DoorCommand;
import tw.teddysoft.monitor.Result;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by teddy on 2017/3/27.
 */
public class CommandTest {
    private DoorCommand doorCmd;

    @Test
    public void result_is_critical_when_door_is_open(){
        doorCmd =  new DoorCommand(new OpenDoorStub());
        Result result = doorCmd.execute();
        assertThat(result.getStatus()).isEqualTo(Result.CRITICAL);
        assertThat(result.getMessage()).startsWith("Door is open");
    }


    @Test
    public void result_is_OK_when_door_is_not_open(){
        doorCmd =  new DoorCommand(new ClosedDoorStub());
        Result result = doorCmd.execute();
        assertThat(result.getStatus()).isEqualTo(Result.OK);
        assertThat(result.getMessage()).startsWith("Your home is safe");
    }

    public class OpenDoorStub implements Door {
        @Override
        public String getStatus() {
            return "open";
        }
    }

    public class ClosedDoorStub implements Door {
        @Override
        public String getStatus() {
            return "not open";
        }
    }
}

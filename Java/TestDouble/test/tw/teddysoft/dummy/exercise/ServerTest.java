package tw.teddysoft.dummy.exercise;

import org.junit.Test;
import tw.teddysoft.monitor.Server;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by teddy on 2017/3/27.
 */
public class ServerTest {

    @Test
    public void command_size_are_two_when_add_two_commands_to_server(){
        Server server = new Server(null);
        assertThat(server.getCommandSize()).isEqualTo(0);

        /*
          TODO: use Dummy object to test server.getComandSize()

            The following code will not work because
            the argument of the addCommand method cannot be null

            server.addCommand(null);
            server.addCommand(null);
            */

        assertThat(server.getCommandSize()).isEqualTo(2);
    }
}

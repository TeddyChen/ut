package tw.teddysoft.dummy.ans;

import org.junit.Test;
import tw.teddysoft.monitor.Command;
import tw.teddysoft.monitor.Result;
import tw.teddysoft.monitor.Server;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by teddy on 2017/3/27.
 */
public class ServerTest {

    @Test
    public void command_size_are_two_when_add_two_commands_to_server(){
        Server server = new Server(null);
        assertThat(server.getCommandSize()).isEqualTo(0);

        server.addCommand(new DummyCommand());
        server.addCommand(new DummyCommand());

        assertThat(server.getCommandSize()).isEqualTo(2);
    }

    public class DummyCommand implements Command {
        @Override
        public Result execute() {
            return null;
        }
    }
}

package tw.teddysoft.monitor;

public interface Alert {
	public void sendAlert(String msg);
    public String getAllSentContent();
}

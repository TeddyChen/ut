package tw.teddysoft.monitor;

public class DoorCommand implements Command {

	private Door _door;
	
	public DoorCommand(Door door){
		_door = door;
	}
	
	@Override
	public Result execute() {

		Result result;
		
		if("open".equals(_door.getStatus())){
			result = new Result(Result.CRITICAL, "Door is open.");
		}
		else
			result = new Result(Result.OK, "Your home is safe.");
		
		return result;
	}

}

package tw.teddysoft.monitor;

public interface Command {

	Result execute();
}

package tw.teddysoft.monitor;

import java.util.LinkedList;
import java.util.List;

public class Server {

	private List<Command> command;
	private Alert alert;
	
	public Server(Alert alert){
		command = new LinkedList<Command>();
		this.alert = alert;
	}
	
	public void monitor(){
		for(Command cmd : command){
			Result result = cmd.execute();
			if(Result.OK != result.getStatus()){
				alert.sendAlert(result.getMessage());
			}
		}
	}

	public void addCommand(Command cmd){
		if(null == cmd)
			throw new IllegalArgumentException("Command cannot be null.");
		command.add(cmd);
	}
	
	public void removeCommand(Command cmd){
		command.remove(cmd);
	}
	
	public int getCommandSize(){
		return command.size();
	}
	
	
	
	
}

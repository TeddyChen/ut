package com.adaptionsoft.games.trivia;

import com.adaptionsoft.games.trivia.uglytrivia.Game;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class GameTest {

	@Test
	public void when_game_is_created() throws Exception {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(stream);
		System.setOut(printStream);

		Game game = new Game();

		assertEquals("", stream.toString());
	}

	@Test
	public void when_one_player_is_added_his_name_and_player_number_are_written() throws Exception {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(stream);
		System.setOut(printStream);

		String expected = "Teddy was added\n" +
				"They are player number 1\n";

		Game game = new Game();
		game.add("Teddy");

		assertEquals(expected, stream.toString());
	}

	@Test
	public void when_two_players_are_added_their_name_and_player_number_are_written() throws Exception {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(stream);
		System.setOut(printStream);

		String expected = "Teddy was added\n" +
				"They are player number 1\n" +
				"Eiffel was added\n" +
				"They are player number 2\n";


		Game game = new Game();
		game.add("Teddy");
		game.add("Eiffel");

		assertEquals(expected, stream.toString());
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void when_rolling_dice_without_adding_any_player_an_exception_thrown(){

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(stream);
		System.setOut(printStream);

		String expected = "";

		Game game = new Game();
		game.roll(1);

		assertEquals(expected, stream.toString());
	}

	@Test()
	public void when_rolling_dice_1_something_happens(){
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(stream);
		System.setOut(printStream);

		String expected = "Teddy was added\n" +
				"They are player number 1\n" +
				"Teddy is the current player\n" +
				"They have rolled a 1\n" +
				"Teddy's new location is 1\n" +
				"The category is Science\n" +
				"Science Question 0\n";

		Game game = new Game();
		game.add("Teddy");
		game.roll(1);

		assertEquals(expected, stream.toString());
	}

	@Test()
	public void when_rolling_dice_2_something_happens(){
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(stream);
		System.setOut(printStream);

		String expected = "Teddy was added\n" +
				"They are player number 1\n" +
				"Teddy is the current player\n" +
				"They have rolled a 2\n" +
				"Teddy's new location is 2\n" +
				"The category is Sports\n" +
				"Sports Question 0\n";

		Game game = new Game();
		game.add("Teddy");
		game.roll(2);

		assertEquals(expected, stream.toString());
	}

	@Test()
	public void when_rolling_dice_3_something_happens(){
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(stream);
		System.setOut(printStream);

		String expected = "Teddy was added\n" +
				"They are player number 1\n" +
				"Teddy is the current player\n" +
				"They have rolled a 3\n" +
				"Teddy's new location is 3\n" +
				"The category is Rock\n" +
				"Rock Question 0\n";

		Game game = new Game();
		game.add("Teddy");
		game.roll(3);

		assertEquals(expected, stream.toString());
	}

	@Test()
	public void when_rolling_dice_4_something_happens(){
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(stream);
		System.setOut(printStream);

		String expected = "Teddy was added\n" +
				"They are player number 1\n" +
				"Teddy is the current player\n" +
				"They have rolled a 4\n" +
				"Teddy's new location is 4\n" +
				"The category is Pop\n" +
				"Pop Question 0\n";

		Game game = new Game();
		game.add("Teddy");
		game.roll(4);

		assertEquals(expected, stream.toString());
	}

}

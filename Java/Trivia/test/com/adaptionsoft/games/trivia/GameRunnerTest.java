package com.adaptionsoft.games.trivia;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * Created by teddy on 2017/4/23.
 */
public class GameRunnerTest {

    @Test
    public void check_GameRunner_against_GoldenMaster() throws IOException {
        GoldenMaster goldenMaster = new GoldenMaster();

        for(long seed = 0; seed < 1000; seed++){
            String expected = goldenMaster.getGoldenMaster(seed);
            String actual = goldenMaster.getGameRunnerResult(seed);

            assertEquals(expected, actual);
        }
    }
}

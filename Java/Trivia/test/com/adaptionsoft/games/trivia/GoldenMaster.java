package com.adaptionsoft.games.trivia;

import com.adaptionsoft.games.trivia.runner.GameRunner;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.util.Random;

/**
 * Created by teddy on 2017/4/23.
 */
public class GoldenMaster {

    //Step 1
    public String getGameRunnerResult(long seed) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(stream);
        System.setOut(printStream);

        GameRunner.play(new Random(seed));
        return stream.toString();
    }

    //Step 2
    public void generateGoldenMaster() throws IOException {
        for(long seed = 0; seed < 1000; seed++)
            FileUtils.writeStringToFile(new File("goldenMasterData/" + seed + ".txt"),
                    getGameRunnerResult(seed),
                    Charset.defaultCharset());
    }


//    Step 3: generate golden master only once
    	@Test
	public void toGenerateGoldenMaster() throws Exception {
		GoldenMaster master = new GoldenMaster();
		master.generateGoldenMaster();
	}


    //Step 4
    public String getGoldenMaster(long seed) throws IOException {
            return FileUtils.readFileToString(new File("goldenMasterData/" + seed + ".txt"),
                    Charset.defaultCharset());
    }

}




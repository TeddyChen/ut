package tw.teddysoft.ut.abc.ans;

import org.junit.Test;

import java.util.Stack;

import static org.junit.Assert.assertEquals;

/**
 * Created by teddy on 2017/3/27.
 */
public class StackTest {

    @Test
    public void testPop() {
        Stack<String> stack = new Stack<>();
        stack.push("teddy");
        stack.push("kay");
        stack.push("eiffel");
        stack.push("ada");

        assertEquals("ada", stack.pop());
    }
}

package tw.teddysoft.ut.assertion.exercise;

import org.junit.Test;
import tw.teddysoft.ut.refactoring.exercise.Invoice;
import tw.teddysoft.ut.refactoring.exercise.InvoiceBuilder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


/**
 * Created by teddy on 2017/3/2.
 */
public class WhenAssertingInvoice {

    @Test
    public void should_be_a_regular_invoice_when_given_normal_vatRate_and_taxIncludedPrice() {
        Invoice invoice = InvoiceBuilder.newInstance().
                withVatRate(0.05).
                withTaxIncludedPrice(17000).
                issue();

        // use junit to perform assertion
        assertNotNull(invoice);
        assertEquals(17000, invoice.getTaxIncludedPrice());
        assertEquals(810, invoice.getVat());
        assertEquals(16190, invoice.getTaxExcludedPrice());
        assertEquals(0.05, invoice.getVatRate(), 0.001);

        // use hamcrest to perform assertion
        org.hamcrest.MatcherAssert.assertThat(invoice, is(notNullValue()));
        org.hamcrest.MatcherAssert.assertThat(invoice.getTaxIncludedPrice(), is(17000));
        org.hamcrest.MatcherAssert.assertThat(invoice.getVat(), is(810));
        org.hamcrest.MatcherAssert.assertThat(invoice.getTaxExcludedPrice(), is(16190));
        org.hamcrest.MatcherAssert.assertThat(invoice.getVatRate(), is(0.05));


        // use AssertJ to perform assertion
        assertThat(invoice).isNotNull();
        assertThat(invoice.getTaxIncludedPrice()).isEqualTo(17000);
        assertThat(invoice.getVat()).isEqualTo(810);
        assertThat(invoice.getTaxExcludedPrice()).isEqualTo(16190);
        assertThat(invoice.getVatRate()).isEqualTo(0.05);
    }



    @Test
    public void should_pay_1_dollar_tax_when_vatRate_is_5_percent_and_taxExcludedPrice_is_10() {
        Invoice invoice = InvoiceBuilder.newInstance().
                withVatRate(0.05).
                withTaxExcludedPrice(10).
                issue();

        // TODO: use hamcrest and AssertJ to perform assertion

    }

}


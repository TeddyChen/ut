package tw.teddysoft.ut.params.ans;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by teddy on 2017/3/27.
 */

@RunWith(value = Parameterized.class)
public class WhenGettingMinValue {

    @Parameterized.Parameters
    public static List<Object []> data() {
        return Arrays.asList(new Object [][] {
            {-1, -1, 999},
            {-1000, -1000, -4},
            {-5, -5, 0},
            {100, 100, 101},
            {0, 0, 200},
            {-1, -1, 1},
            {0, 0, 0,},
            {1, 1, 2} });
    }

    private int expected;
    private int first;
    private int second;

    public WhenGettingMinValue(int expected, int first, int second){
        this.expected = expected;
        this.first = first;
        this.second = second;
    }

    @Test
    public void show_how_to_test_min_with_the_data_driven_way(){
        assertEquals(expected, Math.min(first, second));
    }
}

package tw.teddysoft.ut.refactoring;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created by teddy on 2017/3/2.
 */
public class WhenIssuingInvoice {

    @Test
    public void should_be_a_regular_invoice_when_given_normal_vatRate_and_taxIncludedPrice() {
        Invoice invoice = InvoiceBuilder.newInstance().
                withVatRate(0.05).
                withTaxIncludedPrice(17000).
                issue();

        assertThat(invoice).isNotNull();
        assertThat(invoice.getTaxIncludedPrice()).isEqualTo(17000);
        assertThat(invoice.getVat()).isEqualTo(810);
        assertThat(invoice.getTaxExcludedPrice()).isEqualTo(16190);
        assertThat(invoice.getVatRate()).isEqualTo(0.05);
    }



    @Test
    public void should_pay_1_dollar_tax_when_vatRate_is_5_percent_and_taxExcludedPrice_is_10() {
        Invoice invoice = InvoiceBuilder.newInstance().
                withVatRate(0.05).
                withTaxExcludedPrice(10).
                issue();

        assertThat(invoice).isNotNull();
        assertThat(invoice.getTaxIncludedPrice()).isEqualTo(11);
        assertThat(invoice.getVat()).isEqualTo(1);
        assertThat(invoice.getTaxExcludedPrice()).isEqualTo(10);
        assertThat(invoice.getVatRate()).isEqualTo(0.05);
    }


    /*
         TODO: This test case will reveal a bug in the InvoiceBuilder class. How to fix it?
     */
    @Test
    public void should_be_tax_free_when_vatRate_is_5_percent_and_taxIncludedPrice_is_10() {
        Invoice invoice = InvoiceBuilder.newInstance().
                withVatRate(0.05).
                withTaxIncludedPrice(10).
                issue();

        assertThat(invoice).isNotNull();
        assertThat(invoice.getTaxIncludedPrice()).isEqualTo(10);
        assertThat(invoice.getVat()).isEqualTo(0);
        assertThat(invoice.getTaxExcludedPrice()).isEqualTo(10);
        assertThat(invoice.getVatRate()).isEqualTo(0.05);
    }

}
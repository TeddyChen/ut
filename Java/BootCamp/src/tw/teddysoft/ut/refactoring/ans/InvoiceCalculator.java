package tw.teddysoft.ut.refactoring.ans;

/**
 * Created by teddy on 2017/3/27.
 */
public class InvoiceCalculator {

    public static int getVAT(int taxIncludedPrice,double vatRate) {
        return  taxIncludedPrice - getTaxExcludedPrice(taxIncludedPrice, vatRate);
    }

    public static int getTaxExcludedPrice(int taxIncludedPrice,double vatRate) {
        return (int) Math.round(taxIncludedPrice / (1 + vatRate));
    }

    public static int getTaxIncludedPrice(int taxExcludedPrice,double vatRate) {
        return (int) Math.round(taxExcludedPrice * vatRate);
    }

}

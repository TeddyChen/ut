package tw.teddysoft.ut.refactoring.ans;

/**
 * Created by teddy on 2017/2/21.
 */
public final class Invoice {
    private final double vatRate;
    private final int taxIncludedPrice;
    private final int vat;
    private final int taxExcludedPrice;

    public Invoice(int taxIncludedPrice, double vatRate, int vat, int taxExcludedPrice) {
        this.taxIncludedPrice = taxIncludedPrice;
        this.vatRate = vatRate;
        this.vat = vat;
        this.taxExcludedPrice = taxExcludedPrice;
    }

    public int getVat() {
        return vat;
    }

    public int getTaxExcludedPrice() {
        return taxExcludedPrice;
    }


    public double getVatRate(){
        return vatRate;
    }

    public int getTaxIncludedPrice() {
        return taxIncludedPrice;
    }
}

package tw.teddysoft.ut.refactoring;

/**
 * Created by teddy on 2017/2/22.
 */
public class InvoiceBuilder {

    private double vatRate = 0;
    private int taxIncludedPrice = 0;
    private int taxExcludedPrice = 0;

    public InvoiceBuilder(){
    }

    public static InvoiceBuilder newInstance() {
        return new InvoiceBuilder();
    }

    public InvoiceBuilder withVatRate(double vatRate){
        this.vatRate = vatRate;
        return this;
    }

    public InvoiceBuilder withTaxIncludedPrice(Integer taxIncludedPrice) {
        this.taxIncludedPrice = taxIncludedPrice;
        taxExcludedPrice = 0;
        return this;
    }

    public InvoiceBuilder withTaxExcludedPrice(Integer taxExcludedPrice) {
        this.taxExcludedPrice = taxExcludedPrice;
        taxIncludedPrice = 0;
        return this;
    }

    public Invoice issue(){
        if(taxIncludedPrice==0 && taxExcludedPrice!=0) {
            taxIncludedPrice =(int) Math.round(taxExcludedPrice * (1 + vatRate));
        }
        return new Invoice(taxIncludedPrice, vatRate,
                getVAT(),
                getTaxExcludedPrice());
    }

    private int getTaxExcludedPrice(){
        return (int) Math.round(taxIncludedPrice / (1 + vatRate));
    }

    private int getTaxIncludedPrice() {
        return (int) Math.round(taxExcludedPrice * (1 + vatRate));
    }

    private int getVAT(){
        return (int) Math.round(getTaxExcludedPrice() * vatRate);
    }

}

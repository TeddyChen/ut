package tw.teddysoft.ut;

/**
 * Created by teddy on 2017/3/27.
 */
public class AccessDeniedException extends Exception {

        public AccessDeniedException(){
            super();
        }

    public AccessDeniedException(String message){
        super(message);
    }

    }

package tw.teddysoft.ut;

/**
 * Created by teddy on 2017/3/26.
 */
public class Account {
    private static final String SECRET_TOKEN = "@!teddysoft*&_";
    private String id;
    private String password;
    private double balance;

    public Account(String id, String password, double balance){
        this.id = id;
        this.password = password;
        this.balance = balance;
    }

    public String getId(){
        return id;
    }

    public String getPassword(String secretToken) throws AccessDeniedException {
        if (null == secretToken)
            throw new IllegalArgumentException("The Secret Token cannot be null.");

        if (SECRET_TOKEN.equals(secretToken)){
            return password;
        }
        else {
            throw new AccessDeniedException("Invalid secret token.");
        }
    }

    public double getBalance(){
        return balance;
    }

    public boolean withdraw(double amount){
        if (amount > 0 && balance >= amount){
            balance = balance - amount;
            return true;
        }
        return false;
    }

    public boolean deposit(double amount){
        if (amount > 0){
            balance = balance + amount;
            return true;
        }
        return false;
    }

}

package tw.teddysoft.ut.exercise;

/**
 * Created by teddy on 2017/3/27.
 */
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import tw.teddysoft.ut.Account;

import static org.junit.Assert.*;

/**
 * Created by teddy on 2017/3/26.
 */
public class AccountTest {
    private static final double ACCEPTABLE_DELTA = 1.0;
    private static final double  INIT_BALANCE = 20000;
    private static final String PASSWORD = "1234";
    Account account;

    @Before
    public void setUp(){
        account = new Account("teddy",PASSWORD, INIT_BALANCE);
    }

    @After
    public void tearDown(){
        // nothing to clenaup
    }


    @Test
    public void achieve_100_percent_statement_coverage_when_test_withdraw(){
        // TODO: Test the withdraw method to achieve 100% statement (line) coverage
    }

    @Test
    public void achieve_100_percent_branch_coverage_when_test_withdraw(){
        // TODO: Test the withdraw method to achieve 100% branch coverage
    }

    @Test
    public void consider_boundary_conditions_when_test_withdraw(){
        // TODO: Test withdraw method by considering boundary conditions
    }

    /*
        TODO: Think about that do we need to add this test case
        to achieve 100% line coverage for the Account class?
     */
//    @Test
//    public void testId(){
//        assertEquals("teddy", account.getId());
//    }


    /*
        TODO: Is this test method name a good one?
     */
    @Test
    public void testDeposit() {
        // A normal deposit amount
        assertTrue(account.deposit(500));
        assertEquals(20500, account.getBalance(), ACCEPTABLE_DELTA);

        // A negative deposit amount which is unacceptable
        assertFalse(account.deposit(-300));
        assertEquals(20500, account.getBalance(), ACCEPTABLE_DELTA);

        // A zero deposit amount which is unacceptable
        assertFalse(account.deposit(0));
        assertEquals(20500, account.getBalance(), ACCEPTABLE_DELTA);

    }


}
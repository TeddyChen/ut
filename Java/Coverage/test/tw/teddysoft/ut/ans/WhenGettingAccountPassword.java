package tw.teddysoft.ut.ans;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import tw.teddysoft.ut.AccessDeniedException;
import tw.teddysoft.ut.Account;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;


/**
 * Created by teddy on 2017/3/27.
 */
public class WhenGettingAccountPassword {
    private static final String PASSWORD = "1234";
    Account account;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp(){
        account = new Account("teddy",PASSWORD, 20000);
    }

    @Test
    public void the_JUnit3_way_to_test_exception() {
        try {
            account.getPassword("trytogetpwd");
            fail("Expected an AccessDeniedException to be thrown");
        } catch (AccessDeniedException e) {
            assertEquals("Invalid secret token.", e.getMessage());
        }
    }

    @Test(expected = AccessDeniedException.class)
    public void thrown_an_exception_when_access_password_with_a_wrong_secret_token() throws AccessDeniedException {
        account.getPassword("trytogetpwd");
    }

    @Test
    public void successfully_get_password_when_providing_the_correct_secret_token() throws AccessDeniedException {
        assertEquals(PASSWORD, account.getPassword("@!teddysoft*&_"));
    }

    @Test
    public void test_exception_whit_Rule() throws AccessDeniedException {
        thrown.expect(AccessDeniedException.class);
        thrown.expectMessage("Invalid secret token");
        account.getPassword("trytogetpwd");
    }

    @Test
    public void thrown_an_IllegalArgumentException_when_providing_null_secret_token_v1() throws AccessDeniedException {
        try {
            account.getPassword(null);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals("The Secret Token cannot be null.", e.getMessage());
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void thrown_an_IllegalArgumentException_when_providing_null_secret_token_v2() throws AccessDeniedException {
        account.getPassword(null);
    }

    @Test
    public void thrown_an_IllegalArgumentException_when_providing_null_secret_token_v3() throws AccessDeniedException {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("The Secret Token cannot be null.");
        account.getPassword(null);
    }
}

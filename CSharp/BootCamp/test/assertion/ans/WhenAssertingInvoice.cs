﻿using NUnit.Framework;
using System;
using Tw.Teddysoft.BootCamp.Assertion;
using NFluent;

namespace Tw.Teddysoft.BootCamp.Test.Assertion.Ans
{
	/**
    *   Created by teddy at Teddysoft
    */
	[TestFixture()]
    public class WhenAssertingInvoice
    {

		[Test()]
    	public void Should_be_a_regular_invoice_when_given_normal_vatRate_and_taxIncludedPrice()
		{
			Invoice invoice = InvoiceBuilder.NewInstance().
					WithVatRate(0.05).
					WithTaxIncludedPrice(17000).
					Issue();

			// use Nunit to perform assertions
            Assert.NotNull(invoice);
			Assert.AreEqual(17000, invoice.GetTaxIncludedPrice());
			Assert.AreEqual(810, invoice.GetVat());
			Assert.AreEqual(16190, invoice.GetTaxExcludedPrice());
			Assert.AreEqual(0.05, invoice.GetVatRate(), 0.001);


			// use NFluent to perform assertion
			Check.That(invoice).IsNotNull();
			Check.That(invoice.GetTaxIncludedPrice()).IsEqualTo(17000);
			Check.That(invoice.GetVat()).IsEqualTo(810);
			Check.That(invoice.GetTaxExcludedPrice()).IsEqualTo(16190);
			Check.That(invoice.GetVatRate()).IsEqualTo(0.05);
		}


        [Test()]
    	public void Should_pay_1_dollar_tax_when_vatRate_is_5_percent_and_taxExcludedPrice_is_10()
		{
			Invoice invoice = InvoiceBuilder.NewInstance().
					WithVatRate(0.05).
					WithTaxExcludedPrice(10).
					Issue();

			// use NFluent to perform assertions
			Check.That(invoice).IsNotNull();
			Check.That(invoice.GetTaxIncludedPrice()).IsEqualTo(11);
			Check.That(invoice.GetVat()).IsEqualTo(1);
			Check.That(invoice.GetTaxExcludedPrice()).IsEqualTo(10);
			Check.That(invoice.GetVatRate()).IsEqualTo(0.05);
		}

    }
}

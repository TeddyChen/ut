﻿﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Tw.Teddysoft.BootCamp.Test.Abc.Ans
{
	/**
    *   Created by teddy at Teddysoft
    */
	[TestFixture()]
    public class StackTest
    {
        [Test()]
        public void TestPop()
        {
			Stack<string> stack = new Stack<string>();
            stack.Push("teddy");
			stack.Push("kay");
			stack.Push("eiffel");
			stack.Push("ada");

            Assert.AreEqual("ada", stack.Pop());
        }
    }
}
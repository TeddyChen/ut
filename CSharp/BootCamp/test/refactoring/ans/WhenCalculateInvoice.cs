﻿using System;
using NUnit.Framework;
using NFluent;
using Tw.Teddysoft.BootCamp.Refactoring.Ans;

namespace Tw.Teddysoft.BootCamp.Test.Refactoring.Ans
{
	/**
    *   Created by teddy at Teddysoft
    */
	public class WhenCalculateInvoice
    {
        private readonly double VAT_RATE = 0.05;

		[Test()]
	    public void vat_should_be_zero_when_tax_included_price_is_0_and_vat_rat_is_5_percent()
		{
			Check.That(InvoiceCalculator.GetVAT(0, VAT_RATE)).IsEqualTo(0);
		}

		[Test()]
	    public void vat_should_be_zero_when_tax_included_price_is_10_and_vat_rat_is_5_percent()
		{
			Check.That(InvoiceCalculator.GetVAT(10, VAT_RATE)).IsEqualTo(0);
		}

		[Test()]
	    public void vat_should_be_1_when_tax_included_price_is_11_and_vat_rat_is_5_percent()
		{
			Check.That(InvoiceCalculator.GetVAT(11, VAT_RATE)).IsEqualTo(1);
		}

		[Test()]
	    public void tax_exclude_price_should_be_10_when_tax_included_price_is_10_and_vat_rat_is_5_percent()
		{
			Check.That(InvoiceCalculator.GetTaxExcludedPrice(10, VAT_RATE)).IsEqualTo(10);
		}

		[Test()]
	    public void tax_exclude_price_should_be_10_when_tax_included_price_is_11_and_vat_rat_is_5_percent()
		{
			Check.That(InvoiceCalculator.GetTaxExcludedPrice(10, VAT_RATE)).IsEqualTo(10);
		}
    }
}

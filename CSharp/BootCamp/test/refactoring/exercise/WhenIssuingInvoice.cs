﻿using System;
using NUnit.Framework;
using NFluent;
using Tw.Teddysoft.BootCamp.Refactoring.Exercise;

namespace Tw.Teddysoft.BootCamp.Test.Refactoring.Exercise
{
	/**
    *   Created by teddy at Teddysoft
    */
	public class WhenIssuingInvoice
    {
		[Test()]
		public void should_be_a_regular_invoice_when_given_normal_vatRate_and_taxIncludedPrice()
		{
			Invoice invoice = InvoiceBuilder.NewInstance().
					WithVatRate(0.05).
					WithTaxIncludedPrice(17000).
					Issue();

			Check.That(invoice).IsNotNull();
			Check.That(invoice.GetTaxIncludedPrice()).IsEqualTo(17000);
			Check.That(invoice.GetVat()).IsEqualTo(810);
			Check.That(invoice.GetTaxExcludedPrice()).IsEqualTo(16190);
			Check.That(invoice.GetVatRate()).IsEqualTo(0.05);
		}


		[Test()]
		public void should_pay_1_dollar_tax_when_vatRate_is_5_percent_and_taxExcludedPrice_is_10()
		{
			Invoice invoice = InvoiceBuilder.NewInstance().
					WithVatRate(0.05).
					WithTaxExcludedPrice(10).
					Issue();

			Check.That(invoice).IsNotNull();
			Check.That(invoice.GetTaxIncludedPrice()).IsEqualTo(11);
			Check.That(invoice.GetVat()).IsEqualTo(1);
			Check.That(invoice.GetTaxExcludedPrice()).IsEqualTo(10);
			Check.That(invoice.GetVatRate()).IsEqualTo(0.05);
		}


		/*
             TODO: This test case will reveal a bug in the InvoiceBuilder class. How to fix it?
         */
		[Test()]
		public void should_be_tax_free_when_vatRate_is_5_percent_and_taxIncludedPrice_is_10()
		{
			Invoice invoice = InvoiceBuilder.NewInstance().
					WithVatRate(0.05).
					WithTaxIncludedPrice(10).
					Issue();

			Check.That(invoice).IsNotNull();
			Check.That(invoice.GetTaxIncludedPrice()).IsEqualTo(10);
			Check.That(invoice.GetVat()).IsEqualTo(0);
			Check.That(invoice.GetTaxExcludedPrice()).IsEqualTo(10);
			Check.That(invoice.GetVatRate()).IsEqualTo(0.05);
		}
	}
}

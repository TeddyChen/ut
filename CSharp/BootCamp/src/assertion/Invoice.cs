﻿using System;

namespace Tw.Teddysoft.BootCamp.Assertion
{
	/**
    *   Created by teddy at Teddysoft
    */
	public sealed class Invoice
    {
		private readonly double vatRate;
		private readonly int taxIncludedPrice;
		private readonly int vat;
		private readonly int taxExcludedPrice;

		public Invoice(int taxIncludedPrice, double vatRate, int vat, int taxExcludedPrice)
		{
			this.taxIncludedPrice = taxIncludedPrice;
			this.vatRate = vatRate;
			this.vat = vat;
			this.taxExcludedPrice = taxExcludedPrice;
		}

		public int GetVat()
		{
			return vat;
		}

		public int GetTaxExcludedPrice()
		{
			return taxExcludedPrice;
		}


		public double GetVatRate()
		{
			return vatRate;
		}

		public int GetTaxIncludedPrice()
		{
			return taxIncludedPrice;
		}

	}
}
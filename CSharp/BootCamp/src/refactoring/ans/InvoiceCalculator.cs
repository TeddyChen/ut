﻿using System;

namespace Tw.Teddysoft.BootCamp.Refactoring.Ans
{
	/**
    *   Created by teddy at Teddysoft
    */
	public class InvoiceCalculator
	{
		public static int GetVAT(int taxIncludedPrice, double vatRate)
		{
			return taxIncludedPrice - GetTaxExcludedPrice(taxIncludedPrice, vatRate);
		}

		public static int GetTaxExcludedPrice(int taxIncludedPrice, double vatRate)
		{
			return (int)Math.Round(taxIncludedPrice / (1 + vatRate), MidpointRounding.AwayFromZero);
		}

		public static int GetTaxIncludedPrice(int taxExcludedPrice, double vatRate)
		{
			return (int)Math.Round(taxExcludedPrice * vatRate, MidpointRounding.AwayFromZero);
		}

	}
}

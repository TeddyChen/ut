﻿using System;

namespace Tw.Teddysoft.BootCamp.Refactoring.Ans
{
	/**
    *   Created by teddy at Teddysoft
    */
	public class InvoiceBuilder
	{
		private double vatRate = 0;
		private int taxIncludedPrice = 0;
		private int taxExcludedPrice = 0;

		public InvoiceBuilder()
		{
		}

		public static InvoiceBuilder NewInstance()
		{
			return new InvoiceBuilder();
		}

		public InvoiceBuilder WithVatRate(double vatRate)
		{
			this.vatRate = vatRate;
			return this;
		}

		public InvoiceBuilder WithTaxIncludedPrice(int taxIncludedPrice)
		{
			this.taxIncludedPrice = taxIncludedPrice;
			this.taxExcludedPrice = 0;
			return this;
		}

		public InvoiceBuilder WithTaxExcludedPrice(int taxExcludedPrice)
		{
			this.taxExcludedPrice = taxExcludedPrice;
			this.taxIncludedPrice = 0;
			return this;
		}

		public Invoice Issue()
		{
			if (taxIncludedPrice == 0 && taxExcludedPrice != 0)
			{
				taxIncludedPrice = (int)Math.Round(taxExcludedPrice * (1 + vatRate), MidpointRounding.AwayFromZero);
			}

			return new Invoice(taxIncludedPrice, vatRate,
					InvoiceCalculator.GetVAT(taxIncludedPrice, vatRate),
					InvoiceCalculator.GetTaxExcludedPrice(taxIncludedPrice, vatRate));
		}

	}
}

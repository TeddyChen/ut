﻿using System;

namespace TW.Teddysoft.TestDouble.Monitor
{
	/**
    *   Created by teddy at Teddysoft
    */
	public interface IAlert
    {
		void SendAlert(string msg);
		string GetAllSentContent();
    }
}

﻿using System;

namespace TW.Teddysoft.TestDouble.Monitor
{
	/**
    *   Created by teddy at Teddysoft
    */
	public interface ICommand
    {
        Result Execute();
    }
}

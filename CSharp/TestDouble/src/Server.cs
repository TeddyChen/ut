﻿using System;
using System.Collections.Generic;

namespace TW.Teddysoft.TestDouble.Monitor
{
	/**
    *   Created by teddy at Teddysoft
    */
	public class Server
    {
		private ICollection<ICommand> command;
		private IAlert alert;

		public Server(IAlert alert)
		{
			command = new LinkedList<ICommand>();
			this.alert = alert;
		}

		public void Monitor()
		{
			foreach (ICommand cmd in command)
			{
				Result result = cmd.Execute();
				if (Result.OK != result.GetStatus())
				{
					alert.SendAlert(result.GetMessage());
				}
			}
		}

		public void AddCommand(ICommand cmd)
		{
			if (null == cmd)
				throw new ArgumentException("Command cannot be null.");
			command.Add(cmd);
		}

		public void RemoveCommand(ICommand cmd)
		{
			command.Remove(cmd);
		}

		public int GetCommandSize()
		{
            return command.Count;
		}
    }
}

﻿using System;

namespace TW.Teddysoft.TestDouble.Monitor
{
	/**
    *   Created by teddy at Teddysoft
    */
	public class DoorCommand: ICommand
    {
		private IDoor _door;

		public DoorCommand(IDoor door)
		{
			_door = door;
		}

		public Result Execute()
		{
			Result result;

			if ("open".Equals(_door.GetStatus()))
			{
				result = new Result(Result.CRITICAL, "Door is open.");
			}
			else
				result = new Result(Result.OK, "Your home is safe.");

			return result;
		}

	}
}

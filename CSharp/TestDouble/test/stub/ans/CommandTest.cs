﻿using System;
using NUnit.Framework;
using NFluent;
using TW.Teddysoft.TestDouble.Monitor;

namespace TW.Teddysoft.TestDouble.Stub.Ans
{
	/**
    *   Created by teddy at Teddysoft
    */
	[TestFixture()]
    public class CommandTest
    {
        private DoorCommand doorCmd;

        [Test()]
        public void result_is_critical_when_door_is_open()
        {
            doorCmd = new DoorCommand(new OpenDoorStub());
            Result result = doorCmd.Execute();
            Check.That(result.GetStatus()).IsEqualTo(Result.CRITICAL);
            Check.That(result.GetMessage()).StartsWith("Door is open");
        }


        [Test()]
        public void result_is_OK_when_door_is_not_open()
        {
            doorCmd = new DoorCommand(new ClosedDoorStub());
            Result result = doorCmd.Execute();
            Check.That(result.GetStatus()).IsEqualTo(Result.OK);
            Check.That(result.GetMessage()).StartsWith("Your home is safe");
        }

        public class OpenDoorStub : IDoor
        {
            public String GetStatus()
            {
                return "open";
            }
        }

        public class ClosedDoorStub : IDoor
        {

            public String GetStatus()
            {
                return "not open";
            }
        }
    }
}

﻿using System;
using System.IO;
using TW.Teddysoft.TestDouble.Monitor;

namespace TW.Teddysoft.TestDouble.Fake
{
	/**
    *   Created by teddy at Teddysoft
    */
	public class FakeLogAlert : IAlert
    {
        private readonly string fileName;

	    public FakeLogAlert(string fileName)
		{
	        this.fileName = fileName;
		}

		public void SendAlert(string msg)
		{
	    	msg = msg + "\n";

			using (FileStream fs = new FileStream(fileName, FileMode.Append, FileAccess.Write))
			using (StreamWriter sw = new StreamWriter(fs))
			{
				sw.WriteLine(msg);
			}

	    }

		public String GetAllSentContent()
		{
			string readText = File.ReadAllText(fileName);
			return readText;
		}
    }
}

﻿using System;
using System.IO;
using NUnit.Framework;
using NFluent;
using TW.Teddysoft.TestDouble.Monitor;
using System.Reflection;
using System.Text;

namespace TW.Teddysoft.TestDouble.Fake
{
	/**
    *   Created by teddy at Teddysoft
    */
	[TestFixture()]
    public class WhenGettiingAllSentContentByAlert
    {
        private static readonly string FILE_NAME = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "/fakelog.txt";

	    [SetUp]
		public void setUp()
		{
            if (File.Exists(FILE_NAME))
            {
                File.Delete(FILE_NAME);
            }
            File.Create(FILE_NAME).Dispose();
	    }

		[Test()]
		public void should_get_three_items_when_add_three_door_open_commands() 
		{
			FakeLogAlert alert = new FakeLogAlert(FILE_NAME);
		    Server server = new Server(alert);
		    server.AddCommand(new DoorCommand(new OpenDoorStub()));
		    server.AddCommand(new DoorCommand(new OpenDoorStub()));
		    server.AddCommand(new DoorCommand(new OpenDoorStub()));
		    server.Monitor();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Door is open.\n");
            sb.AppendLine("Door is open.\n");
            sb.AppendLine("Door is open.\n");

            Check.That(alert.GetAllSentContent()).IsEqualTo(sb.ToString());
	    }


	    [Test()]
		public void should_get_empty_content_when_add_three_door_closed_commands() 
	    {
			FakeLogAlert alert = new FakeLogAlert(FILE_NAME);
	    	Server server = new Server(alert);
		    server.AddCommand(new DoorCommand(new ClosedDoorStub()));
	        server.AddCommand(new DoorCommand(new ClosedDoorStub()));
	        server.AddCommand(new DoorCommand(new ClosedDoorStub()));
	        server.Monitor();

			Check.That(alert.GetAllSentContent()).IsEqualTo("");
	    }

	    public class OpenDoorStub : IDoor
	    {
			public String GetStatus()
	        {
		        return "open";
	        }
	    }

	    public class ClosedDoorStub : IDoor
	    {
			public String GetStatus()
	        {
		        return "not open";
	        }
	    }
    }
}

﻿using System;
using NUnit.Framework;
using NFluent;
using Moq;
using TW.Teddysoft.TestDouble.Monitor;

namespace TW.Teddysoft.TestDouble.Mock.Ans
{
	/**
    *   Created by teddy at Teddysoft
    */
    [TestFixture()]
	public class ServerTest
    {
        [Test]
        public void command_size_are_two_when_add_two_commands_to_server()
        {
            var mockCmd = new Mock<ICommand>();
            var mockAlert = new Mock<IAlert>();
            Server server = new Server(mockAlert.Object);
            Check.That(server.GetCommandSize()).IsEqualTo(0);

            server.AddCommand(mockCmd.Object);
            server.AddCommand(mockCmd.Object);

            Check.That(server.GetCommandSize()).IsEqualTo(2);
        }
    }
}

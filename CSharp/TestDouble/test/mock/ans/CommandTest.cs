﻿using System;
using NUnit.Framework;
using NFluent;
using Moq;
using TW.Teddysoft.TestDouble.Monitor;

namespace TW.Teddysoft.TestDouble.Mock.Ans
{
	/**
    *   Created by teddy at Teddysoft
    */
    [TestFixture()]
	public class CommandTest
    {
        [Test]
	    public void result_is_critical_when_door_is_open()
		{
		    var mock = new Mock<ICommand>();
		    mock.Setup(cmd => cmd.Execute()).Returns(new Result(Result.CRITICAL, "Door is open"));

            Result status = mock.Object.Execute();
			Check.That(status.GetStatus()).IsEqualTo(Result.CRITICAL);
            Check.That(status.GetMessage()).Equals("Door is open");
        }


		[Test]	
        public void result_is_OK_when_door_is_not_open()
		{
			var mock = new Mock<ICommand>();
			mock.Setup(cmd => cmd.Execute()).Returns(new Result(Result.OK, "Your home is safe"));

			Result status = mock.Object.Execute();
			Check.That(status.GetStatus()).IsEqualTo(Result.OK);
			Check.That(status.GetMessage()).Equals("Your home is safe");
        }
    }
}

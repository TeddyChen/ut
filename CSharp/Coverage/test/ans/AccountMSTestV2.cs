﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tw.Teddysoft.Coverage.Domain;

namespace Tw.Teddysoft.Coverage.Test.Ans
{
    [TestClass]
    public class AccountMSTestV2Test
    {
        private static readonly double INIT_BALANCE = 20000;
        Account account;

        [TestInitialize]
        public void SetUp()
        {
            account = new Account("teddy", "1234", INIT_BALANCE);
        }

        [TestCleanup]
        public void TearDown()
        {
            // nothing to clenaup
        }

        [TestMethod]
        public void achieve_100_percent_statement_coverage_when_test_withdraw()
        {
            Assert.IsTrue(account.Withdraw(1000));
            Assert.AreEqual(19000, account.GetBalance());

            Assert.IsFalse(account.Withdraw(-1));
            Assert.AreEqual(19000, account.GetBalance());
        }

        [TestMethod]
        public void achieve_100_percent_branch_coverage_when_test_withdraw()
        {
            Assert.IsTrue(account.Withdraw(1000));
            Assert.IsFalse(account.Withdraw(-1));
            Assert.IsFalse(account.Withdraw(30000));
        }

        [TestMethod]
        public void consider_boundary_conditions_when_test_withdraw()
        {
            Assert.IsTrue(account.Withdraw(19999));
            Assert.IsTrue(account.Withdraw(1));
            Assert.IsFalse(account.Withdraw(1));
        }

        /*
			Is this test method name a good one?
		 */
        [TestMethod]
        public void testDeposit()
        {
            // A normal deposit amount
            Assert.IsTrue(account.Deposit(500));
            Assert.AreEqual(20500, account.GetBalance());

            // A negative deposit amount which is unacceptable
            Assert.IsFalse(account.Deposit(-300));
            Assert.AreEqual(20500, account.GetBalance());

            // A zero deposit amount which is unacceptable
            Assert.IsFalse(account.Deposit(0));
            Assert.AreEqual(20500, account.GetBalance());

        }

        [TestMethod]
        public void testId()
        {
            Assert.AreEqual("teddy", account.GetId());
        }
    }
}

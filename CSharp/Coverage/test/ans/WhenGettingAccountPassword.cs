﻿using System;
using NUnit.Framework;
using Tw.Teddysoft.Coverage.Domain;

namespace Tw.Teddysoft.Coverage.Test.Ans
{
	/**
    *   Created by teddy at Teddysoft
    */
	[TestFixture]
    public class WhenGettingAccountPassword
    {
        private static readonly string PASSWORD = "1234";
        Account account;

        [SetUp]
        public void SetUp()
        {
            account = new Account("teddy", PASSWORD, 20000);
        }

        [Test]
        public void the_xUnit3_way_to_test_exception()
        {
            try
            {
                account.GetPassword("trytogetpwd");
                Assert.Fail("Expected an AccessDeniedException to be thrown");
            }
            catch (AccessDeniedException e)
            {
                Assert.AreEqual("Invalid secret token.", e.Message);
            }
        }

        [Test]
        public void successfully_get_password_when_providing_the_correct_secret_token()
        {
            Assert.AreEqual(PASSWORD, account.GetPassword("@!teddysoft*&_"));
        }

        [Test]
        public void test_exception_whit_Assert_Thorws()
        {
            var ex = Assert.Throws<Coverage.Domain.AccessDeniedException>(() => account.GetPassword("trytogetpwd"));
            Assert.AreEqual(ex.Message, "Invalid secret token.");

        }

        [Test]
        public void thrown_an_IllegalArgumentException_when_providing_null_secret_token_v1()
        {
            try {
                account.GetPassword(null);
                Assert.Fail("Expected an IllegalArgumentException to be thrown");
            } catch (ArgumentException e) {
                Assert.AreEqual("The Secret Token cannot be null.", e.Message);
            }
        }

        [Test]
        public void thrown_an_ArgumentException_when_providing_null_secret_token_v3() 
        {
			var ex = Assert.Throws<ArgumentException>(() => account.GetPassword(null));
			Assert.AreEqual(ex.Message, "The Secret Token cannot be null.");
        }
    }
}
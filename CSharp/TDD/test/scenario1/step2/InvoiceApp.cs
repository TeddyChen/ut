﻿using System;

namespace Tw.Teddysoft.TDD.Scenario1.Step2
{
    /**
     * Created by teddy at Teddysoft
     */
    public class InvoiceApp
    {
        private int taxIncludedPrice = 0;
        private double vatRate = 0;

        public void stop()
        {
        }

        public void start()
        {
        }

        public void collectingInput()
        {
        }

        public int getTaxIncludedPrice()
        {
            return taxIncludedPrice;
        }

        public double getVatRate()
        {
            return vatRate;
        }

        //public void show(Invoice invoice)
        //{
        //}
    }
}

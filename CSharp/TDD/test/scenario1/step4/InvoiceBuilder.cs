﻿using System;

namespace Tw.Teddysoft.TDD.Scenario1.Step4
{
	/**
    *   Created by teddy at Teddysoft
    */
	public class InvoiceBuilder
    {
		public static InvoiceBuilder newInstance()
		{
			return new InvoiceBuilder();
		}

		public InvoiceBuilder withTaxIncludedPrice(int taxIncludedPrice)
		{
			return this;
		}

		public InvoiceBuilder withVatRate(double vatRate)
		{
			return this;
		}

		public Invoice issue()
		{
			return null;
		}
    }
}

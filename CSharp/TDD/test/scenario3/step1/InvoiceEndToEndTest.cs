﻿using System;
using NUnit.Framework;
using NFluent;

namespace Tw.Teddysoft.TDD.Scenario3.Step1
{
    /**
     * Created by teddy at Teddysoft
     */
    public class InvoiceEndToEndTest
    {
            InvoiceApp app;
        
            [SetUp]
            public void SetUp(){
                app = new InvoiceApp();
            }
        
            [TearDown]
            public void TearDown(){
                if (null != app)
                    app.stop();
            }
        
            [Test]
            public void issuing_invoice_with_tax_included_price(){
		        app.start();
		        app.collectingInput(17000, 0.05);

		        int taxIncludedPrice = app.getTaxIncludedPrice();
		        double vatRate = app.getVatRate();

		        Invoice invoice = InvoiceBuilder.newInstance().
		                withTaxIncludedPrice(taxIncludedPrice).
		                withVatRate(vatRate).
		                issue();

		        Check.That(invoice).IsNotNull();
		        Check.That(invoice.getTaxIncludedPrice()).IsEqualTo(17000);
		        Check.That(invoice.getVat()).IsEqualTo(810);
		        Check.That(invoice.getTaxExcludedPrice()).IsEqualTo(16190);
		        Check.That(invoice.getVatRate()).IsEqualTo(0.05);

		        app.show(invoice);
            }

            [Test]
            public void issuing_invoice_with_tax_included_price_round_example()
            {
		        app.start();
		        app.collectingInput(99, 0.05);

		        int taxIncludedPrice = app.getTaxIncludedPrice();
		        double vatRate = app.getVatRate();

		        Invoice invoice = InvoiceBuilder.newInstance().
		                withTaxIncludedPrice(taxIncludedPrice).
		                withVatRate(vatRate).
		                issue();

		        Check.That(invoice).IsNotNull();
		        Check.That(invoice.getTaxIncludedPrice()).IsEqualTo(99);
		        Check.That(invoice.getVat()).IsEqualTo(5);
		        Check.That(invoice.getTaxExcludedPrice()).IsEqualTo(94);
		        Check.That(invoice.getVatRate()).IsEqualTo(0.05);

		        app.show(invoice);
            }

            // This test case fails. In step 2, write unit test to verify the InvoiceBuilder logic
            [Test]
            public void should_be_tax_free_when_tax_included_price_is_10(){
		        app.start();
		        app.collectingInput(10, 0.05);

		        int taxIncludedPrice = app.getTaxIncludedPrice();
		        double vatRate = app.getVatRate();

		        Invoice invoice = InvoiceBuilder.newInstance().
		                withTaxIncludedPrice(taxIncludedPrice).
		                withVatRate(vatRate).
		                issue();

		        Check.That(invoice).IsNotNull();
		        Check.That(invoice.getTaxIncludedPrice()).IsEqualTo(10);
		        Check.That(invoice.getVat()).IsEqualTo(0);
		        Check.That(invoice.getTaxExcludedPrice()).IsEqualTo(10);
		        Check.That(invoice.getVatRate()).IsEqualTo(0.05);

		        app.show(invoice);
        }

    }
}

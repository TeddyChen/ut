﻿using System;

namespace Tw.Teddysoft.TDD.Scenario3.Step1
{
    /**
    *   Created by teddy at Teddysoft
    */
    public class InvoiceBuilder
    {
        private int taxIncludedPrice;
        private double vatRate;

        public static InvoiceBuilder newInstance()
        {
            return new InvoiceBuilder();
        }

        public InvoiceBuilder withTaxIncludedPrice(int taxIncludedPrice)
        {
            this.taxIncludedPrice = taxIncludedPrice;
            return this;
        }

        public InvoiceBuilder withVatRate(double vatRate)
        {
            this.vatRate = vatRate;
            return this;
        }

        public Invoice issue()
        {
            return new Invoice(taxIncludedPrice,
                    calcVat(taxIncludedPrice, vatRate),
                    calcTaxExcludedPrice(taxIncludedPrice, vatRate), vatRate);
        }

        public static int calcVat(int taxIncludedPrice, double vatRate)
        {
            return (int)Math.Round(calcTaxExcludedPrice(taxIncludedPrice, vatRate) * vatRate, MidpointRounding.AwayFromZero);
        }

        public static int calcTaxExcludedPrice(int taxIncludedPrice, double vatRate)
        {
            return (int)Math.Round(taxIncludedPrice / (1 + vatRate), MidpointRounding.AwayFromZero);
        }
    }
}

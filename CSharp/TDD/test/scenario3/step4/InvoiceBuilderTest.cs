﻿using System;
using NUnit.Framework;
using NFluent;

namespace Tw.Teddysoft.TDD.Scenario3.Step4
{
	/**
	* Created by teddy at Teddysoft
	*/
	public class InvoiceBuilderTest
    {
		[Test]
	    public void calc_tax_excluded_price_with_tax_included_price()
		{
			// expected = 94.28 -> 94
			Check.That(InvoiceBuilder.calcTaxExcludedPrice(99, 0.05)).IsEqualTo(94);

			// expected = 104.76 -> 105
			Check.That(InvoiceBuilder.calcTaxExcludedPrice(110, 0.05)).IsEqualTo(105);
		}

		[Test]
        public void calcVat_with_tax_included_price()
		{
			Check.That(InvoiceBuilder.calcVat(99, 0.05)).IsEqualTo(5);
			Check.That(InvoiceBuilder.calcVat(110, 0.05)).IsEqualTo(5);
		}

        [Test]
        public void when_tax_included_price_under_10_then_tax_free()
        {
            Check.That(InvoiceBuilder.calcVat(0, 0.05)).IsEqualTo(0);
            Check.That(InvoiceBuilder.calcVat(1, 0.05)).IsEqualTo(0);
            Check.That(InvoiceBuilder.calcVat(2, 0.05)).IsEqualTo(0);
            Check.That(InvoiceBuilder.calcVat(3, 0.05)).IsEqualTo(0);
            Check.That(InvoiceBuilder.calcVat(4, 0.05)).IsEqualTo(0);
            Check.That(InvoiceBuilder.calcVat(5, 0.05)).IsEqualTo(0);
            Check.That(InvoiceBuilder.calcVat(6, 0.05)).IsEqualTo(0);
            Check.That(InvoiceBuilder.calcVat(7, 0.05)).IsEqualTo(0);
            Check.That(InvoiceBuilder.calcVat(8, 0.05)).IsEqualTo(0);
            Check.That(InvoiceBuilder.calcVat(9, 0.05)).IsEqualTo(0);
            Check.That(InvoiceBuilder.calcVat(10, 0.05)).IsEqualTo(0);
        }

        [Test]
        public void when_tax_included_price_is_11_then_vat_is_1()
        {
            Check.That(InvoiceBuilder.calcVat(11, 0.05)).IsEqualTo(1);
        }

    }
}

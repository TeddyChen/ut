﻿using System;
using NFluent;
using NUnit.Framework;

namespace TDD.test.clean.scenario1.step3
{
    public class IssueInvoiceUseCaseTest
    {
        [Test]
        public void withTaxIncludedPrice()
        {
            IssueInvoiceUseCase issueInvoiceUseCase = new IssueInvoiceUseCase();

            IssueInvoiceInput input = new IssueInvoiceInput();
            input.setTaxIncludedPrice(17000);
            input.setVatRate(0.05);

            IssueInvoiceOutput output = new IssueInvoiceOutput();

            issueInvoiceUseCase.execute(input, output);

            Check.That(output.getTaxIncludedPrice()).IsEqualTo(17000);
            Check.That(output.getVat()).IsEqualTo(810);
            Check.That(output.getTaxExcludedPrice()).IsEqualTo(16190);
            Check.That(output.getVatRate()).IsEqualTo(0.05);

        }

    }
}

﻿namespace TDD.test.clean.scenario1.step3
{
    public class IssueInvoiceUseCase
    {
        public void execute(IssueInvoiceInput input, IssueInvoiceOutput output)
        {
            output.setTaxIncludedPrice(input.getTaxIncludedPrice());
            output.setVatRate(input.getVatRate());
            output.setTaxExcludedPrice(16190);
            output.setVat(810);
        }
    }
}
﻿namespace TDD.test.clean.scenario2.step4
{
    public class IssueInvoiceUseCase
    {
        public void execute(IssueInvoiceInput input, IssueInvoiceOutput output)
        {
                    InvoiceBuilder builder = new InvoiceBuilder();
                    builder.withTaxIncludedPrice(input.getTaxIncludedPrice());
                    builder.withVatRate(input.getVatRate());
                    Invoice invoice = builder.issue();
            
                    output.setTaxIncludedPrice(invoice.getTaxIncludedPrice());
                    output.setVatRate(invoice.getVatRate());
                    output.setTaxExcludedPrice(invoice.getTaxExcludedPrice());
                    output.setVat(invoice.getVat());

        }
    }
}
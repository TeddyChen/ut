﻿namespace TDD.test.clean.scenario2.step1
{
    public class IssueInvoiceInput
    {
        private int taxIncludedPrice;
        private double vatRate;

        public void setTaxIncludedPrice(int taxIncludedPrice)
        {
            this.taxIncludedPrice = taxIncludedPrice;
        }

        public int getTaxIncludedPrice()
        {
            return taxIncludedPrice;
        }

        public void setVatRate(double vatRate)
        {
            this.vatRate = vatRate;
        }

        public double getVatRate()
        {
            return vatRate;
        }
    }
}
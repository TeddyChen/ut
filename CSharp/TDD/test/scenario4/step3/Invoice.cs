﻿using System;

namespace Tw.Teddysoft.TDD.Scenario4.Step3
{
    /**
     * Created by teddy at Teddysoft
     */    
    public class Invoice
    {
	    private int taxIncludedPrice;
	    private int vat;
	    private int taxExcludedPrice;
	    private double vatRate;

	    public Invoice(int taxIncludedPrice, int vat, int taxExcludedPrice, double vatRate) {
	        this.taxIncludedPrice = taxIncludedPrice;
	        this.vat = vat;
	        this.taxExcludedPrice = taxExcludedPrice;
	        this.vatRate = vatRate;

	    }

	    public int getTaxIncludedPrice() {
	        return taxIncludedPrice;
	    }

	    public int getVat() {
	        return vat;
	    }

	    public int getTaxExcludedPrice() {
	        return taxExcludedPrice;
	    }

	    public double getVatRate() {
	        return vatRate;
	    }
	    }
}
